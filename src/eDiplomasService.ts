import { ApplicationService } from '@themost/common';
import { ODataModelBuilder } from '@themost/data';
import { Router, Application } from 'express';
import { eDiplomasRouter } from './eDiplomasRouter';
import { LocalDepartmentReplacer } from './LocalDepartmentReplacer';
import { StudentReplacer } from './StudentReplacer';
import { StudyProgramSpecialtyReplacer } from './StudyProgramSpecialtyReplacer';

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
 function insertRouterBefore(parent: Router, before: Router, insert: Router) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}


// tslint:disable-next-line: class-name
export class eDiplomasService extends ApplicationService {
    constructor(app: any) {
        super(app);
        // extend LocalDepartment
        new LocalDepartmentReplacer(app).apply();
        // extend StudyProgramSpecialty
        new StudyProgramSpecialtyReplacer(app).apply();

        new StudentReplacer(app).apply();

        // refresh builder
        const builder = app.getService(ODataModelBuilder);
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }

        if (app && app.container) {
            app.container.subscribe((container: Application) => {
                if (container) {
                    // get container router
                    const router = container._router;
                    // find after position
                    const before = router.stack.find((item: Router) => {
                        return item.name === 'dataContextMiddleware';
                    });
                    // use ediplomas router
                    container.use('/ediplomas', eDiplomasRouter(app));
                    if (before == null) {
                        // do nothing
                        return;
                    }
                    // get last router
                    const insert = router.stack[router.stack.length - 1];
                    // insert (re-index) router
                    insertRouterBefore(router, before, insert);
                }
            });
        }
    }
}