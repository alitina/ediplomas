import { TextUtils } from '@themost/common';
import { DataContext, DataObject, DataObjectState, DataPermissionEventListener, EdmMapping, EdmType } from '@themost/data';
import {promisify} from 'es6-promisify';
import {readStream} from '@themost/express';
import { Workbook } from 'exceljs';

declare interface ImportDegreeTemplate {
    id?: string;
    titleID: number;
    title?: string;
    studyProgramName?: string;
    departmentCode?: string;
    studyLevelCode?: number;
    instituteName?: string;
    publisherName?: string;
    dateCreated?: Date;
    dateModified?: Date;
}

function parseInteger(value: any): number | undefined {
    return parseInt(value, 10);
}

function parseString(value: any): string | undefined {
    if (value == null) {
        return;
    }
    return value.toString();
}

const headerMappings: any[][] = [
    [ 'Κωδικός Template', 'titleID' ],
    [ 'Τίτλος', 'title' ],
    [ 'Πρόγραμμα Σπουδών_Όνομα', 'studyProgramName' ],
    [ 'Πρόγραμμα Σπουδών_Κωδικός Πανελληνίων', 'departmentCode' ],
    [ 'Πρόγραμμα Σπουδών_Επίπεδο', 'studyLevelCode' ],
    [ 'Ίδρυμα_Όνομα', 'instituteName' ],
    [ 'Αρμόδιο Τμήμα', 'departmentName' ],
    [ 'Τμήμα Έκδοσης_Όνομα', 'publisherName' ]
];

const valueFormatters: any[][] = [
    [ 'titleID', parseString ],
    [ 'departmentCode', parseString ],
    [ 'studyLevelCode', parseInteger ]
];

@EdmMapping.entityType('DegreeTemplate')
class DegreeTemplate extends DataObject {

    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('preImport', EdmType.CollectionOf('Object'))
    static async preImport(context: DataContext, file: any) {

        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: context.model('DegreeTemplate'), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        let blob: any;
        if (file instanceof Buffer) {
            blob = file;
        } else if (file instanceof ArrayBuffer) {
            blob = new Uint8Array(file);
        } else {
            blob = await readStream(file);
        }
        const results: ImportDegreeTemplate[] = [];
        const workbook = new Workbook();
        await workbook.xlsx.load(blob);
        const worksheet = workbook.getWorksheet(1);
        worksheet.eachRow((row: any, rowNumber: number) => {
            if (rowNumber === 1) {
                // get header values
                const headerValues = row.values as any[];
                headerValues.forEach((v: any, k: number) => {
                    // find header mapping (if any)
                    const find = headerMappings.find((headerMapping) => {
                        return headerMapping[0] === v;
                    });
                    if (find) {
                        // and hold column index
                        find.push(k);
                    }
                });
            } else {
                const res: any = { };
                // get row values
                const values = row.values as any[];
                if (values != null) {
                    // enumerate header mapping (with indexes)
                    headerMappings.filter((headerMapping) => {
                        return headerMapping[2] != null;
                    }).forEach((headerMapping) => {
                        const k = headerMapping[2];
                        // set property name and values
                        const formatter = valueFormatters.find((valueFormatter) => {
                            return valueFormatter[0] === headerMapping[1];
                        });
                        if (formatter) {
                            const formatterFunc: (value: any) => string = formatter[1];
                            res[headerMapping[1]] = formatterFunc(values[k]);
                        } else {
                            res[headerMapping[1]] = values[k];
                        }
                    });
                }
                Object.assign(res, {
                    dateCreated: new Date(),
                    dateModified: new Date()
                });
                results.push(res);
            }
        });
        return results;
    }

}

export {
    DegreeTemplate
}