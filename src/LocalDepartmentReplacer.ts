import { ApplicationService, Guid } from '@themost/common';
import { DataObject, DataQueryable, EdmMapping, EdmType, ModelClassLoaderStrategy, SchemaLoaderStrategy } from '@themost/data';
class LocalDepartment extends DataObject {
    @EdmMapping.func('DegreeTemplates', EdmType.CollectionOf('Object'))
    async getDegreeTemplates(): Promise<DataQueryable> {
        const alternativeCode = await this.getModel().where('id').equal(this.getId()).select('alternativeCode').value();
        if (alternativeCode != null) {
            return this.context.model('DegreeTemplate').where('departmentCode').equal(alternativeCode).prepare();
        }
        // create an empty query
        return this.context.model('DegreeTemplate').where('departmentCode').equal(Guid.newGuid().toString()).prepare();
    }
}

class LocalDepartmentReplacer extends ApplicationService {
    constructor(app: any) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy as new () => SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('LocalDepartment');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy as new () => ModelClassLoaderStrategy);
        const LocalDepartmentBase = loader.resolve(model) as any;
        // extend class
        Object.assign(LocalDepartmentBase.prototype, {
            getDegreeTemplates: LocalDepartment.prototype.getDegreeTemplates
        });
    }

}

export {
    LocalDepartment,
    LocalDepartmentReplacer
}